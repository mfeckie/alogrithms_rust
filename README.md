# Algorithms in Rust #

This repo is my attempt at implementing the algorithms discussed in **Algorithms** 4th ed, (Sedgewick & Wayne)

### Build Requirements ###

* Rust 0.11

### Contribution guidelines ###

* Writing tests -> Yes, do it!
* Be nice
use std::os;
use std::io::File;
fn get_path_to_file() -> Path{
  if os::args().len() < 2 {
    fail!("No filename given"); 
  } else {
     Path::new(os::args().get(1).clone())
  }
}

fn open_file(path: Path) {
    let mut file = match File::open(&path) {
        Err(why) => fail!("Failed to open file, {}: {}", path.display(), why.desc),
        Ok(file) => file,
    };

    match file.read_to_str() {
        Err(why) => fail!("Could not read : {}", why.desc),
        Ok(string) => print!("File contains: \n{}", string),
    }
}

fn main () {
  open_file(get_path_to_file());
}


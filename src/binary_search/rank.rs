pub fn rank(key: int, values: &[int]) -> int {

    let mut lo = 0u;

    let mut hi = values.len() - 1u ;

    while lo <= hi {
        let mid: uint = lo + (hi - lo) / 2;
        if key < values[mid] {
            hi = mid - 1;
        } else if key > values[mid] {
            lo = mid + 1;
        } else {
            return mid as int
        }
    }
    return -1;
}

#[test]
fn returns_position_of_element_when_present(){
    let input = [1i,2,3,4,5];
    assert_eq!(rank(3, input), 2);
}

#[test]
fn returns_minus_one_for_failure(){
    let input = [1i,2,3,4,5];
    assert_eq!(rank(6, input), -1);
}

